## Utwórz 3 aplikacje:

#### APP1
* APP1 pobiera pogodę z API z **6** miast raz na **10 minut**  po czym zapisuje dane na topicu wather

```
Palo Alto
San Francisco
Raleigh
London
Frankfurt
New York
```

* zapytanie o miasta powinno być parapmtryzowane (ZMIENNA ŚRODOWISKOWA LUB PLIK KONFIGURACYJNY)


#### APP2

pobiera dane Utkowników z topicu **users** oraz pogodę z topicu **wather** - łączy dane na topicu users-wather

* dane powinny być w avro

#### APP3

pobiera WYDARZENIA  z topicu **events** oraz pogodę z topicu **wather** - łączy dane na topicu events-wather. 
Powinna nastąpić zastąpienie adresów IP nazwami miast.

* dane powinny być w avro

#### Palo Alto
```
111.152.45.45
111.203.236.146
111.168.57.122
111.249.79.93
```
#### San Francisco
```
111.168.57.122
111.90.225.227
111.173.165.103
111.145.8.144
111.245.174.248
111.245.174.111
222.152.45.45
222.203.236.146
222.168.57.122
222.249.79.93
```
#### Raleigh
```
222.168.57.122
222.90.225.227
222.173.165.103
222.145.8.144
222.245.174.248
222.245.174.222
122.152.45.245
122.203.236.246
122.168.57.222
```
#### London
```
122.249.79.233
122.168.57.222
122.90.225.227
122.173.165.203
```
#### Frankfurt
```
122.145.8.244
122.245.174.248
122.245.174.122
233.152.245.45
233.203.236.146
233.168.257.122
233.249.279.93
233.168.257.122
```
#### New York
```
233.90.225.227
233.173.215.103
233.145.28.144
233.245.174.248
233.245.174.233
```


![Alt text](zadanie.jpg)


## Utwórz obrazy aplikacji

zbuduj obrazy ora wypchnij je do reposzytorium devopstst.azurecr.io

## przygotuj deploymenty

przygotuj deploymenty aplikacji do klastra k8s
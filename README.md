## How to cahnge cluster


Veryfiy of Your context
```
kubectl config get-contexts
```
use docker-desktop to use local cluster

```
kubectl config use-context docker-desktop
```

# Create pod / replica set / deployment

create pod - use devops/k8s-local/Deployment/pod.yaml 
* first change pod name in line 4
* use kubectl appaly -f path/to/file.yaml


create replicaset - use devops/k8s-local/Deployment/rs.yaml 

create deployment - use devops/k8s-local/Deployment/deployment.yaml

create different deplyment
* first change pod name in line 4
* use kubectl appaly -f path/to/file.yaml

## change in Deployment 
change image in deployment

```
kubectl set image deployment/nginx-deployment bar-container-name=httpd
```

check deatails of pod by use 

```
kubectl describe pods <<pod name>>
```

change image in deployment fo your Favourite image (I suer if have one! If Forgot can use ubuntu)


scale deployment to 6 replicas
```
kubectl scale --replicas 6 deployment nginx-deployment
```


## rollback

play with rollback

```
kubectl rollout history deployment.v1.apps/nginx-deployment

kubectl rollout history deployment.v1.apps/nginx-deployment --revision=2
```

rollback to the previous revision
```
kubectl rollout undo deployment.v1.apps/nginx-deployment
```

rollback to a specific revision
```
kubectl rollout undo deployment.v1.apps/nginx-deployment --to-revision=1
```

## namespace

create namesapce

```
kubectl create namespace namesapce-name

kubectl get namespace
```


add namesapce in deployment.yaml in metadata section
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  namespace: namesapce-name
  labels:
    app: nginx
    [...]
```

change default namesapce
```
kubectl config set-context --current --namespace namesapce-name
```


## rolong update vs recreate


use deployment devops/k8s-local/Deployment/deployment-Recreate.yaml

change image and watch

```
kubectl set image deployment/nginx-deployment bar-container-name=httpd

kubectl set image deployment/nginx-deployment bar-container-name=nginx
```

use deployment devops/k8s-local/Deployment/deployment-roll.yaml

change image and watch

```
kubectl set image deployment/nginx-deployment bar-container-name=httpd

kubectl set image deployment/nginx-deployment bar-container-name=nginx
```

# service

prepare services

```
kubectl apply -f devops/k8s-local/Service/deployment-service.yaml

kubectl apply -f devops/k8s-local/Service/service.yaml
```

test it form inside of pods


```
kubectl exec -it pod-name -- bash

curl cat.default.svc.cluster.local

curl cat
```


### secret

create sceret for private container repository

```
kubectl create secret docker-registry <name> --docker-server=DOCKER_REGISTRY_SERVER --docker-username=DOCKER_USER --docker-password=DOCKER_PASSWORD --docker-email=DOCKER_EMAIL

```



edit cats.yaml - add Your Cats apliaction - and deploy it

```
kubectl apply -f k8s-local/Service/deployment-service.yaml

# add to pod sec
 
spec:
  containers:
  imagePullSecrets:
  - name: <secret name >

```

## volume


create configmap from file and use it
```
kubectl create configmap index --from-file=devops/k8s-local/Storage/index.html

kubectl get configmaps

kubectl apply -f devops/k8s-local/Storage/deployment-devops-cat3.yaml

kubectl exec cat-deployment-<xxxx>-<xxx> -- ls /usr/share/nginx/html

kubectl exec cat-deployment-<xxxx>-<xxx> -- curl cat
```


create secret and use it
```
kubectl create secret generic dog --from-file=devops/k8s-local/Storage/dog.jpeg

kubectl get secrets

kubectl apply -f devops/k8s-local/Storage/deployment-devops-cat4.yaml

kubectl exec cat-deployment-<xxxx>-<xxx> -- ls /usr/share/nginx/html

kubectl exec cat-deployment-<xxxx>-<xxx> -- curl cat

## clean up

``` 
kubectl get all --all-namespaces
kubectl delete <kind> <name>

```